﻿Function ConvertFrom-SecureToPlain {
    PARAM( 
        [Parameter(Mandatory, ValueFromPipeline)]
        [securestring] $SecurePassword
    )
    
    # Create a "password pointer"
    $PasswordPointer = [Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
    
    # Get the plain text version of the password
    $PlainTextPassword = [Runtime.InteropServices.Marshal]::PtrToStringAuto($PasswordPointer)
    
    # Free the pointer
    [Runtime.InteropServices.Marshal]::ZeroFreeBSTR($PasswordPointer)
    
    # Return the plain text password
    $PlainTextPassword
}


# Plain -> Secure
$PasswordFile = './securestring.txt'
$SecureString = Read-Host -AsSecureString -Prompt 'Password' `
    | ConvertFrom-SecureString > $PasswordFile

# Secure -> Plain
Get-Content $PasswordFile `
    | ConvertTo-SecureString `
    | ConvertFrom-SecureToPlain